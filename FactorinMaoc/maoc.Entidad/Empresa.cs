﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace maoc.Entidad
{
    [Table("Empresa")]
    public partial class Empresa
    {
        [Key]
        [Display(Name = "Codigo")]
        public int IdEmpresa { get; set; }
        [Display(Name = "Usuario")]
        public int IdUsuario { get; set; }
        [Display(Name = "RUC")]
        public string RUC { get; set; }
        [Display(Name = "Razon Social")]
        public string Razon { get; set; }
        [Display(Name = "Direccion")]
        public string Direccion { get; set; }
        [Display(Name = "Departamento")]
        public string Departamento { get; set; }
        [Display(Name = "Provincia")]
        public string Provincia { get; set; }
        [Display(Name = "Distrito")]
        public string Distrito { get; set; }
        [Display(Name = "Rubro")]
        public string Rubro { get; set; }
        [Display (Name ="Nombre Usuario")]
        public string NomUser { get; set; }
    }
}
