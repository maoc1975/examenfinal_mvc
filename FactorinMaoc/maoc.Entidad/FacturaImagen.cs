﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace maoc.Entidad
{
    [Table("FacturaImagen")]
    public partial class FacturaImagen
    {
        [Key]
        [Display(Name = "IdFacturaImagen")]
        public int IdFacturaImagen { get; set;}
        [Display(Name = "IdFactura")]
        public int IdFactura { get; set; }
        [Display(Name = "Ubicacion de Imagen")]
        public string Ruta { get; set; }
       
    }
}
