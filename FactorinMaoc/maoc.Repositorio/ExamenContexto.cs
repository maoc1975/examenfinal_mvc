﻿using maoc.Entidad;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace maoc.Repositorio
{
    public partial class ExamenContexto : DbContext
    {
        public ExamenContexto():base("name=Data")
        {

        }

        public virtual DbSet<Empresa> Empresa { get; set; }
        public virtual DbSet<Factura> Factura { get; set; }
        public virtual DbSet<FacturaImagen> FacturaImagen { get; set; }

    }
}
