﻿using maoc.Entidad;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace maoc.Repositorio.Impl
{
    public class ExamenRepositorio : IDisposable 
    {
        private readonly DbContext bd1;

        private readonly RepositorioGenerico<Empresa> _empresa;
        private readonly RepositorioGenerico<Factura> _factura;
        private readonly RepositorioGenerico<FacturaImagen> _facturaImagen;

        public ExamenRepositorio (DbContext bd)
        {
            this.bd1 = bd;
            _empresa = new RepositorioGenerico<Empresa>(bd1);
            _factura = new RepositorioGenerico<Factura>(bd1);
            _facturaImagen = new RepositorioGenerico<FacturaImagen>(bd1);
        }
        public RepositorioGenerico<Empresa> Empresa { get { return _empresa ; } }
        public RepositorioGenerico<Factura> Factura { get { return _factura; } }
        public RepositorioGenerico<FacturaImagen> FacturaImagen { get { return _facturaImagen; } }

        public void Commit()
        {
            bd1.SaveChanges();
        }
        public void Dispose()
        {
            bd1.Dispose();
        }
    }
}
