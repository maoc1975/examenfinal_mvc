namespace maoc.Repositorio.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PrimerRegistro : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Empresa",
                c => new
                    {
                        IdEmpresa = c.Int(nullable: false, identity: true),
                        IdUsuario = c.Int(nullable: false),
                        RUC = c.String(),
                        Razon = c.String(),
                        Direccion = c.String(),
                        Departamento = c.String(),
                        Provincia = c.String(),
                        Distrito = c.String(),
                        Rubro = c.String(),
                    })
                .PrimaryKey(t => t.IdEmpresa);
            
            CreateTable(
                "dbo.Factura",
                c => new
                    {
                        IdFactura = c.Int(nullable: false, identity: true),
                        Numero = c.String(),
                        Fecha_Emision = c.DateTime(nullable: false),
                        Fecha_Vencimiento = c.DateTime(nullable: false),
                        Fecha_Cobro = c.DateTime(nullable: false),
                        RucCliente = c.String(),
                        RazonCliente = c.String(),
                        TotalFactura = c.Double(nullable: false),
                        TotalImpuesto = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.IdFactura);
            
            CreateTable(
                "dbo.FacturaImagen",
                c => new
                    {
                        IdFacturaImagen = c.Int(nullable: false, identity: true),
                        IdFactura = c.Int(nullable: false),
                        Ruta = c.String(),
                    })
                .PrimaryKey(t => t.IdFacturaImagen);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.FacturaImagen");
            DropTable("dbo.Factura");
            DropTable("dbo.Empresa");
        }
    }
}
