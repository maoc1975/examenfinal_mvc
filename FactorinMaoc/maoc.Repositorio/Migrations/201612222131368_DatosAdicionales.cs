namespace maoc.Repositorio.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DatosAdicionales : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Empresa", "NomUser", c => c.String());
            AddColumn("dbo.Factura", "IdEmpresa", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Factura", "IdEmpresa");
            DropColumn("dbo.Empresa", "NomUser");
        }
    }
}
