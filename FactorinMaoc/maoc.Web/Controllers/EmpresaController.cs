﻿using maoc.Funcionalidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace maoc.Web.Controllers
{
    public class EmpresaController : Controller
    {
        // GET: Empresa
        public ActionResult Lista(string userId)
        {
            //int idUser = 11;
            var Listado = new ListadoEmpresaHandler();
            return View(Listado.Listado(userId));
        }
        [HttpGet]
        public ActionResult Registrar(string userId)
        {
            return View(new CrearEmpresaViewModel());
        }

        [HttpPost]
        public ActionResult Registrar(string userId, CrearEmpresaViewModel modelo)
        {
            try
            {
                var CreaEmpresa = new CrearEmpresaHandler();
                modelo.NomUser = userId;
                CreaEmpresa.Ejecutar(modelo);
                return RedirectToAction("Lista",new { UserId = modelo.NomUser });
            }
            catch
            {
                return View();
            }
        }
    }
}