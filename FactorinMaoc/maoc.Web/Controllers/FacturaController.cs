﻿using maoc.Funcionalidades;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace maoc.Web.Controllers
{
    public class FacturaController : Controller
    {
        // GET: Factura
        public ActionResult Lista(int idEmpresax)
        {
            var Listado = new ListarFacturaHandler();
            return View(Listado.Listado(idEmpresax));
        }
    }
}