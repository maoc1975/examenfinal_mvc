﻿using maoc.Entidad;
using maoc.Repositorio;
using maoc.Repositorio.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace maoc.Funcionalidades
{
    public class CrearEmpresaHandler: IDisposable
    {
        private readonly ExamenRepositorio Examen;

        public CrearEmpresaHandler()
        {
            Examen = new ExamenRepositorio(new ExamenContexto());
        }
        public void Dispose()
        {
            Examen.Dispose();
        }
        public void Ejecutar (CrearEmpresaViewModel modelo)
        {
            Empresa empre = CrearEmpresa(modelo);
            Examen.Empresa.Agregar(empre);
            Examen.Commit();
        }
        private Empresa CrearEmpresa(CrearEmpresaViewModel modelo)
        {
            return new Empresa
            {  NomUser = modelo.NomUser,
              RUC = modelo.RUC,
              Rubro = modelo.Rubro,
              Razon=modelo.Razon,
              Direccion = modelo.Direccion,
              Departamento = modelo.Departamento,
              Provincia =modelo.Provincia,
              Distrito =modelo.Distrito
            };

        }
    }
}