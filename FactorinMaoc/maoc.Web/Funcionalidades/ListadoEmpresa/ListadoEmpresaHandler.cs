﻿using maoc.Repositorio;
using maoc.Repositorio.Impl;
using maoc.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace maoc.Funcionalidades
{
    public class ListadoEmpresaHandler :IDisposable
    {
        private readonly ExamenRepositorio Examen;
        
        public ListadoEmpresaHandler()
        {
            Examen = new ExamenRepositorio(new ExamenContexto());
        }

        public void Dispose()
        {
            Examen.Dispose();
        }

        public IEnumerable<ListadoEmpresaViewModel>Listado( string idUsuario)
        {
            //var IdNew = Convert.ToInt32(idUsuario);
            var lista = Examen.Empresa.TraeVarios(e => e.NomUser.Equals(idUsuario));

            return lista.Select(i =>
            new ListadoEmpresaViewModel()
            {
                IdEmpresa = i.IdEmpresa,
                //userId = i.IdUsuario,
                RUC = i.RUC,
                Razon = i.Razon,
                Ubicacion = i.Departamento + "/" + i.Provincia + "/" + i.Distrito,
                Rubro = i.Rubro
            }
            );
            
        }


    }
}