﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace maoc.Funcionalidades
{
    public class ListadoEmpresaViewModel
    {
        [Display(Name = "Codigo")]
        public int IdEmpresa { get; set; }
        //[Display(Name = "Nombre Usuario")]
        //public string NomUser { get; set; }
        [Display(Name = "RUC")]
        public string RUC { get; set; }
        [Display(Name = "Razon Social")]
        public string Razon { get; set; }
        
        [Display(Name = "Ubicación")]
        public string Ubicacion { get; set; }
        [Display(Name = "Rubro")]
        public string Rubro { get; set; }
    }
}