﻿using maoc.Repositorio;
using maoc.Repositorio.Impl;
using maoc.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace maoc.Funcionalidades
{
    public class ListarFacturaHandler : IDisposable 
    {
        private readonly ExamenRepositorio Examen;

        public ListarFacturaHandler()
        {
            Examen = new ExamenRepositorio(new ExamenContexto());
        }
        public void Dispose()
        {
            Examen.Dispose();
        }
        public IEnumerable<ListarFacturaViewModel>Listado(int IdEmpresax)
        {
            var Lista = Examen.Factura.TraeVarios(e => e.IdEmpresa.Equals(IdEmpresax));

            return Lista.Select(i =>
                new ListarFacturaViewModel()
                {
                     IdEmpresa=IdEmpresax,
                     Fecha_Cobro = i.Fecha_Cobro,
                     Fecha_Emision=i.Fecha_Emision,
                     Fecha_Vencimiento = i.Fecha_Vencimiento,
                     Numero = i.Numero,
                     RucCliente = i.RucCliente,
                     RazonCliente = i.RazonCliente,
                     TotalFactura = i.TotalFactura,
                     TotalImpuesto = i.TotalImpuesto
                });
        }
    }
}