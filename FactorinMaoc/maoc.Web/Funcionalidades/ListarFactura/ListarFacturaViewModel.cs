﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace maoc.Funcionalidades
{ 
    public class ListarFacturaViewModel
    {
        [Display(Name = "Id")]
        public int IdFactura { get; set; }
        [Display(Name = "Numero Factura")]
        public string Numero { get; set; }
        [Display(Name = "Fecha Emision")]
        public DateTime Fecha_Emision { get; set; }
        [Display(Name = "Fecha Vencimiento")]
        public DateTime Fecha_Vencimiento { get; set; }
        [Display(Name = "Fecha Cobro")]
        public DateTime Fecha_Cobro { get; set; }
        [Display(Name = "Ruc de Cliente")]
        public string RucCliente { get; set; }
        [Display(Name = "Razon Social Cliente")]
        public string RazonCliente { get; set; }
        [Display(Name = "Total de Factura")]
        public double TotalFactura { get; set; }
        [Display(Name = "Total de Impuesto")]
        public double TotalImpuesto { get; set; }

        [Display(Name = "IdEmpresa")]
        public int IdEmpresa { get; set; }
        [Display (Name = "Nombre Empresa")]
        public string NombreEmpresa { get; set; }

    }
}