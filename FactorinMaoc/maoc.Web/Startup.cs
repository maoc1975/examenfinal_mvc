﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(maoc.Web.Startup))]
namespace maoc.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
